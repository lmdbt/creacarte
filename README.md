# <i class="fa-solid fa-wand-sparkles"></i> CréaCarte <i class="fa-regular fa-address-card"></i> <br> Éditeur de Carte Interactif 

## Introduction
CréaCarte est une application web destinée à concevoir et éditer des cartes personnalisées à imprimer. Elle est idéale pour les enseignants, les créateurs de contenu éducatif, ou toute personne intéressée par la création de supports pédagogiques visuels.

![](capture1.png)

## Fonctionnalités
- **Éditeur en Temps Réel :** Modifiez et visualisez instantanément les changements apportés à vos cartes.
- **Sauvegarde et Exportation :** Sauvegardez vos cartes en format HTML ou imprimez-les directement en PDF.
- **Personnalisation Avancée :** Utilisez diverses options de style pour personnaliser l'apparence de vos cartes.

## Prérequis
- Un navigateur web moderne (Chrome, Firefox, Safari).
- Accès à Internet pour le chargement des ressources externes (Codemirror, Font Awesome).

## Installation
Aucune installation n'est nécessaire. L'application fonctionne directement dans votre navigateur web.

## Utilisation
1. **Édition de carte :** Entrez le contenu HTML directement dans l'éditeur Codemirror à gauche de votre écran.
2. **Visualisation :** Observez le rendu dans la fenêtre de prévisualisation à droite.
3. **Sauvegarde :** Entrez un nom de fichier et cliquez sur "Enregistrer au format HTML" pour télécharger votre travail et éventuellement le reprendre plus tard.
4. **Impression :** Cliquez sur "Imprimer en PDF" pour générer une version imprimable de votre carte.

## Contribution
Les contributions à ce projet sont les bienvenues ! Veuillez soumettre vos bugs, suggestions et demandes de fonctionnalités via le système de tickets GitHub de ce projet.

## Licence
CréaCarte est distribué sous la licence CC-BY. Vous êtes libre de l'utiliser, le modifier, et le distribuer sous les mêmes termes.

