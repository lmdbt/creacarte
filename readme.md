# <b class="fa-solid fa-wand-sparkles"></b> CréaCarte <b class="fa-solid fa-id-card"></b> <br> Un Éditeur de Carte Interactif

## Table des matières

1. [Introduction](#introduction)  
2. [Fonctionnalités](#fonctionnalité) (avec une note d'aide à l'[impression <b class="fa-solid fa-print"></b>](#impression))  
3. [Prérequis/Installation](#install)  
4. [Utilisation](#utilisation)  
5. [Guide de personnalisation](#guide)  
   - [Modifier le texte](#texte)  
   - [Modifier les images](#image)  
   - [Modifier les couleurs des cartes](#couleur)  
   - [Ajouter de nouvelles cartes](#ajout)  
6. [Un exemple : Périmètre et aire de figures usuelles](#exemple)  
7. [Bug connus](#bugs)  
8. [Contribution et licence](#contribution)

<p style="text-align:right;">Documentation au format pdf : [<b class="fa-regular fa-file-pdf"></b>](readme.pdf)</p>

<img src="https://lmdbt.forge.apps.education.fr/creacarte/capture2.png" style="max-width:100%">

<p id="introduction"></p>
## Introduction
CréaCarte est une application web destinée à concevoir et éditer des cartes interactives personnalisées à imprimer. Elle est idéale pour les enseignants, les créateurs de contenu éducatif, ou toute personne intéressée par la création de supports pédagogiques visuels.

CréaCarte a aussi un cousin <a href="https://cartesmd.forge.apps.education.fr/">cartesMD</a> conçu par [Cédric Eyssette](https://eyssette.forge.apps.education.fr/) avec lequel vous pouvez également créer en [Markdown](https://markdown.forge.apps.education.fr/) ce type de support.

<p id="fonctionnalité"></p>
## Fonctionnalités
- **Éditeur en Temps Réel :** Modifiez et visualisez instantanément les changements apportés à vos cartes.  
- **Sauvegarde et Exportation :** Sauvegardez vos cartes en format HTML ou imprimez-les directement en PDF.  
- **Chargement de fichiers enregistrés :** Importez facilement des fichiers `.html` pour reprendre votre travail.  
- **Sauvegarde locale dans le navigateur :** Enregistrez vos projets directement dans votre navigateur pour y accéder ultérieurement.  
- **Personnalisation Avancée :** Utilisez diverses options de style pour personnaliser l'apparence de vos cartes.  
- **Insertion d'icônes :** Utilisez la [version 6 de FontAwesome](https://fontawesome.com/v6/download).  
  Par exemple : `<i class="fa-regular fa-hand-peace"></i>` produira <b class="fa-regular fa-hand-peace"></b>.  
  Retrouvez toutes les icônes disponibles sur [cette page](https://fontawesome.com/v6/search?o=r&m=free).  
- **Écrire des maths :** Utilisez $\LaTeX$ grâce à [Mathjax](https://www.mathjax.org/).  
  Exemple : `$Aire_{triangle}=\dfrac{base\times{}hauteur}{2}$` donne : $Aire_{triangle}=\dfrac{base\times{}hauteur}{2}$.  
- **Mode d'affichage interactif :**  
  - Affichez uniquement le code.  
  - Affichez uniquement les cartes.  
  - Affichez les deux fenêtres côte à côte.  
- <strong id="impression">Impression au format PDF :</strong> Imprimez au format PDF en activant l'impression des arrière-plans (voir image ci-dessous).

<img src="https://lmdbt.forge.apps.education.fr/creacarte/impressionAP.png" alt="Copie d'écran de l'activation des arrière-plans sur Firefox et Brave" style="max-width:100%;">

- **Modèles intégrés :** Deux modèles de cartes sont disponibles :  
  - **Classique** : idéal pour des cartes simples (verso unique).  
  - **FlashCards** : conçu pour des cartes recto-verso avec questions/réponses ou autres contenus interactifs.

<p id="install"></p>
## Prérequis/Installation

### Prérequis

- Un navigateur web moderne (Chrome, Firefox, Safari).
- Accès à Internet pour le chargement des ressources externes (Codemirror, FontAwesome et Mathjax).

### Installation

- Aucune installation n'est nécessaire. L'application fonctionne directement dans votre navigateur web.
- Vous pouvez également télécharger et décompresser [cette archive <b class="fa-solid fa-box-archive"></b>](creacarte.zip), puis ouvrir `index.html` (sans connexion Internet).

<p id="utilisation"></p>
## Utilisation
1. **Lancement de l'application :** Ouvrez le fichier `index.html` dans votre navigateur.  
2. **Édition de carte :** Entrez le contenu HTML dans l'éditeur [Codemirror](https://codemirror.net/) à gauche de votre écran.  
3. **Visualisation :** Observez le rendu dans la fenêtre de prévisualisation à droite.  
4. **Chargement de fichiers :** Cliquez sur "Charger un fichier" pour importer un fichier `.html`.  
5. **Modes d'affichage :** Sélectionnez le mode souhaité (Code uniquement, Aperçu uniquement, ou les deux).  
6. **Sauvegarde :** Enregistrez vos cartes localement ou exportez-les au format HTML.  
7. **Impression :** Cliquez sur "Imprimer en PDF". _Pensez à activer l’[impression des arrière-plans](#impression)._  
8. **Modèles prédéfinis :** Choisissez entre les modèles "Classique" et "FlashCards" selon vos besoins (bouton à droite de l'icône FontAwesome "info").

<p id="guide"></p>
## Guide de Personnalisation

<p id="texte"></p>
### Modification des Textes
1. Localisez le bloc de texte dans l'éditeur HTML.  
2. Modifiez le texte entre les balises appropriées (ex. `<div class="card-title">Votre Titre</div>`).  
3. Les modifications sont visibles immédiatement.

<p id="image"></p>
### Modification des Images
1. Référencez une image en ligne ou utilisez un fichier local.  
2. Placez vos images dans le dossier du fichier HTML.  
3. Modifiez le chemin dans l'attribut `src` ou `background-image`.

#### Ajustement manuel des images de fond
Pour ajuster la taille ou la position d’une image, vous avez deux options :  
- **Option 1 :** Préparer et ajuster vos images au préalable (utiliser un logiciel de retouche comme LibreOffice Impress pour leur donner la taille adéquate). Vous pouvez vous aidez de [cette image](https://forge.apps.education.fr/lmdbt/creacarte/-/blob/main/cadreimage.png?ref_type=heads).  
- **Option 2 :** Pour les utilisateurs avancés, modifier la balise `<div class="card-image">` par ce code :

```html
<div class="card-image"
  style="background-image: url('hibou-calc.png');
         background-size: 75%;
         background-position: 50% 50%;
         background-repeat: no-repeat;
         background-color: LightSteelBlue;">
</div>
```
Ce qui donne ce résultat :

![Capture d'une carte avec l'illustration réduite ajustée](https://lmdbt.forge.apps.education.fr/creacarte/capture-image-ajustée.png)

Vous pouvez ensuite ajuster la taille, la position et la couleur de fond selon vos souhaits.

> **Note :** `LightSteelBlue` fait partie d’une longue liste de couleurs CSS disponibles ici : [CSS Colors - Sur le site W3Schools](https://www.w3schools.com/cssref/css_colors.php)

<p id="couleur"></p>
### Modification des Couleurs des Cartes
1. Sélectionnez la `div` de la carte souhaitée.  
2. Changez la classe pour ajuster la couleur :
   - `card-blue`, `card-yellow`, `card-green`, etc.

<p id="ajout"></p>
#### Ajout de Nouvelles Cartes

- **Première solution :** Depuis la mise à jour, il est possible d’ajouter automatiquement une *paire de cartes* en cliquant sur le bouton <b class="fa-solid fa-circle-plus"></b> dédié.  
- **Deuxième solution :** Copier-coller le code d’une carte existante. **Attention**, les cartes fonctionnent par paires. Il faut donc copier-coller **l’ensemble** du bloc commençant par `<div class="duo-card">` et terminant par `</div>` correspondant à la fin de la **deuxième** carte du duo.


<p id="exemple"></p>
## Quelques exemples

### Modèle classique : Périmètre et aire de figures usuelles

#### Code HTML
[Consulter l'exemple ici <b class="fa-solid fa-file-code"></b>](https://forge.apps.education.fr/lmdbt/creacarte/-/blob/main/exemple_p%C3%A9rim%C3%A8tre_et_aire.html)

#### Aperçu - Avant et après impression
<div style="display: flex;">
  <img src="https://static.piaille.fr/media_attachments/files/112/399/752/630/187/281/original/5afcb0431acab2e3.jpg" width="100%!important" style="max-width:100%" height="auto!important">
  <p>&nbsp;</p>
  <img src="https://static.piaille.fr/media_attachments/files/112/400/527/788/843/982/original/2f67e0162df1d053.jpg" style="max-width:100%" width="100%!important" height="auto!important">
</div>

### Modèle FlashCards : Droites remarquables du triangle

#### Code HTML
[Accéder au modèle modifiable dans CréaCarte <b class="fa-solid fa-arrow-pointer"></b>](https://lmdbt.forge.apps.education.fr/creacarte/droites_triangle.html)

#### Aperçu - Avant impression
![](https://lmdbt.forge.apps.education.fr/creacarte/droites_triangle.png)

<p id="bugs"></p>
## Bugs connus

### Erreur de chargement avec MathJax

- Lorsque le code $\LaTeX$ est un peu complexe, cela peut entrainer une erreur de prise en charge par MathJax au chargement.

- Il est possible de corriger ce problème :
  1. Copier le code du modèle de vos cartes  
  2. Charger le *modèle classique* dans [la page principale de l'application](https://lmdbt.forge.apps.education.fr/creacarte/)  
  3. Réactualiser la page (`Ctrl`+`R`) avec le *modèle classique*.  
  4. Coller le code de vos cartes dans la fenêtre de code de cette page.

<p id="contribution"></p>
## Contribution et licence

### Contribution
Les contributions sont les bienvenues ! Veuillez soumettre vos suggestions via [La Forge](https://forge.apps.education.fr/lmdbt/creacarte/-/issues).

### Licence
CréaCarte est sous licence :
[<img src="https://upload.wikimedia.org/wikipedia/commons/0/0c/MIT_logo.svg" style="max-width:25%">](https://fr.wikipedia.org/wiki/Licence_MIT).  
Vous êtes libre de l'utiliser, de le partager ou de l'intégrer en créditant son auteur : <a href="https://forge.apps.education.fr/users/iaconellicyril/contributed">Cyril Iaconelli</a>